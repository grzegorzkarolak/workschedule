package com.GrzegorzKarolak.workschedule.schedulegenerator.domain;

import java.time.LocalDate;

public class Day {

    private LocalDate day;
    private Boolean isWork;

    public Day(LocalDate day, Boolean isWork) {
        this.day = day;
        this.isWork = isWork;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public Boolean getWork() {
        return isWork;
    }

    public void setWork(Boolean work) {
        isWork = work;
    }
}

package com.GrzegorzKarolak.workschedule.schedulegenerator.domain;

import java.time.LocalDateTime;
import java.util.*;

public class ScheduleForSingleEmployee {

    private Employee employee;
    private List<Day> schedule;

    public ScheduleForSingleEmployee(Employee employee, List<Day> schedule) {
        this.employee = employee;
        this.schedule = schedule;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Day> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<Day> schedule) {
        this.schedule = schedule;
    }
}

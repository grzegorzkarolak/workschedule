package com.GrzegorzKarolak.workschedule.schedulegenerator.domain;

import java.time.LocalDate;
import java.util.List;

public class Employee {

    private String firstName;
    private String lastName;
    private Double workingTimePart;
    private List<LocalDate> holidayDays;

    public Employee(String firstName, String lastName, Double workingTimePart, List<LocalDate> holidayDays) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.workingTimePart = workingTimePart;
        this.holidayDays = holidayDays;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getWorkingTimePart() {
        return workingTimePart;
    }

    public void setWorkingTimePart(Double workingTimePart) {
        this.workingTimePart = workingTimePart;
    }

    public List<LocalDate> getHolidayDays() {
        return holidayDays;
    }

    public void setHolidayDays(List<LocalDate> holidayDays) {
        this.holidayDays = holidayDays;
    }
}

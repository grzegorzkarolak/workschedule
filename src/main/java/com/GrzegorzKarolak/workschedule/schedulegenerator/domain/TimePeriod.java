package com.GrzegorzKarolak.workschedule.schedulegenerator.domain;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimePeriod {

    private LocalDate startDate;
    private LocalDate endDate;

    public TimePeriod(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getDaysPeriod() {
        long periodDaysLong = Duration.between(getStartDate().atStartOfDay(), getEndDate().atStartOfDay()).toDays();
        Integer periodDays = (int) periodDaysLong;
        return periodDays;
    }

    public List<LocalDate> getAllDaysFromPeriod() {
        List<LocalDate> allPeriodDays = new ArrayList<>();
        for (int i = 0; i <= getDaysPeriod(); i++) {
            if (allPeriodDays.isEmpty()) {
                allPeriodDays.add(startDate);
            } else {
                LocalDate nextDay = allPeriodDays.get(i-1).plusDays(1);
                allPeriodDays.add(nextDay);
            }
        }
        return allPeriodDays;
    }
}

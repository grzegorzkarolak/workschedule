package com.GrzegorzKarolak.workschedule.schedulegenerator.domain;

import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ScheduleSettings {

    private Integer fullTimeWorkingTimePerMonthInHours;
    private YearMonth requestedMonthToCalculate;
    private List<DayOfWeek> nonWorkingDays;

    public ScheduleSettings(Integer fullTimeWorkingTimePerMonthInHours, YearMonth requestedMonthToCalculate, List<DayOfWeek> nonWorkingDays) {
        this.fullTimeWorkingTimePerMonthInHours = fullTimeWorkingTimePerMonthInHours;
        this.requestedMonthToCalculate = requestedMonthToCalculate;
        this.nonWorkingDays = nonWorkingDays;
    }

    public Integer getFullTimeWorkingTimePerMonthInHours() {
        return fullTimeWorkingTimePerMonthInHours;
    }

    public void setFullTimeWorkingTimePerMonthInHours(Integer fullTimeWorkingTimePerMonthInHours) {
        this.fullTimeWorkingTimePerMonthInHours = fullTimeWorkingTimePerMonthInHours;
    }

    public YearMonth getRequestedMonthToCalculate() {
        return requestedMonthToCalculate;
    }

    public void setRequestedMonthToCalculate(YearMonth requestedMonthToCalculate) {
        this.requestedMonthToCalculate = requestedMonthToCalculate;
    }

    public List<DayOfWeek> getNonWorkingDays() {
        return nonWorkingDays;
    }

    public void setNonWorkingDays(List<DayOfWeek> nonWorkingDays) {
        this.nonWorkingDays = nonWorkingDays;
    }

    public List<Day> getWorkingDaysOnRequestedPeriod() {
        List<LocalDate> allDays =  getAllDaysFromPeriod(requestedMonthToCalculate.atDay(1), requestedMonthToCalculate.atEndOfMonth());
        Boolean isWorkingDay = false;
        List<Day> result = new ArrayList<>();
        for (LocalDate day : allDays) {
            for (DayOfWeek nonWorkingDay : nonWorkingDays) {
                if(day.getDayOfWeek() == nonWorkingDay) {
                    isWorkingDay = false;
                    break;
                } else {
                    isWorkingDay = true;
                }
            }
            Day dayResult = new Day(day, isWorkingDay);
            result.add(dayResult);
        }
        return result;
    }

    public static List<LocalDate> getAllDaysFromPeriod(LocalDate dayStart, LocalDate dayEnd) {
        long periodInDaysLong = Duration.between(dayStart.atStartOfDay(), dayEnd.atStartOfDay()).toDays();
        Integer periodInDays = (int) periodInDaysLong;
        List<LocalDate> allPeriodDays = new ArrayList<>();
        for (int i = 0; i <= periodInDays; i++) {
            if (allPeriodDays.isEmpty()) {
                allPeriodDays.add(dayStart);
            } else {
                LocalDate nextDay = allPeriodDays.get(i-1).plusDays(1);
                allPeriodDays.add(nextDay);
            }
        }
        return allPeriodDays;
    }
}

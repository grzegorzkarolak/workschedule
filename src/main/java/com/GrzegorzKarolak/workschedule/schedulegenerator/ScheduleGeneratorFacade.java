package com.GrzegorzKarolak.workschedule.schedulegenerator;

import com.GrzegorzKarolak.workschedule.schedulegenerator.domain.*;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

public class ScheduleGeneratorFacade {

    ScheduleForSingleEmployee calculateFor(Employee employee, Integer shiftLength, ScheduleSettings scheduleSettings) {

        YearMonth calculatingYearMonth = 
        Integer employeesWorkingHoursPerMonth = employee.getWorkingTimePart() * scheduleSettings.getFullTimeWorkingTimePerMonthInHours().get()
        Integer requestedPeriodInDays = scheduleSettings.getRequestedMonthToCalculate().getDaysPeriod() + 1;
        Integer employeesWorkingDaysPerMonth = employeesWorkingHoursPerMonth / shiftLength;
        List<LocalDate> requestedPeriodDaysToCalculate = scheduleSettings.getRequestedMonthToCalculate().getAllDaysFromPeriod();

        List<Day> workingDays = getWorkingDays(employee, requestedPeriodInDays, employeesWorkingDaysPerMonth, requestedPeriodDaysToCalculate);

        ScheduleForSingleEmployee calculatedSchedule = new ScheduleForSingleEmployee(employee, workingDays);

        return calculatedSchedule;
    }

    private List<Day> getWorkingDays(Employee employee, Integer requestedPeriodInDays, Integer employeesWorkingDaysPerMonth, List<LocalDate> requestedPeriodDaysToCalculate) {

        List<Day> calculatedDays = new ArrayList<>();
        List<LocalDate> employeesHolidayDays = employee.getHolidayDays();
        Boolean isWorkingDay = false;
        int countWorkingDays = 0;
        for (int i = 0; i < requestedPeriodInDays; i++) {

            for (LocalDate holidayDay : employeesHolidayDays) {
                if (!requestedPeriodDaysToCalculate.get(i).equals(holidayDay)) {
                    if (countWorkingDays < employeesWorkingDaysPerMonth) {
                        isWorkingDay = true;
                    } else {
                        isWorkingDay = false;
                        break;
                    }
                } else {
                    isWorkingDay = false;
                    break;
                }
            }
            if (isWorkingDay) {
                countWorkingDays++;
            }
            Day nextCalculatedDay = new Day(requestedPeriodDaysToCalculate.get(i), isWorkingDay);
            calculatedDays.add(nextCalculatedDay);
        }
        return calculatedDays;
    }
}

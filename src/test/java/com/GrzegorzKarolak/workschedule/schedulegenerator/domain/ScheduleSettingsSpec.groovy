package com.GrzegorzKarolak.workschedule.schedulegenerator.domain

import spock.lang.Specification

import java.time.DayOfWeek
import java.time.Month
import java.time.YearMonth

class ScheduleSettingsSpec extends Specification {

    def "Should generate list of days marked which are working"() {
        given: "Month to calculate - 02.2020, " +
                "full-time hourly work time - 160, " +
                "non working days - sundays"
        ScheduleSettings settings = new ScheduleSettings(160, YearMonth.of(2020, Month.FEBRUARY), new ArrayList<DayOfWeek>(DayOfWeek.SUNDAY))

        when: "for calculation use properties of ScheduleSettings object"
        settings.getWorkingDaysOnRequestedPeriod()

        then: ""
    }
}

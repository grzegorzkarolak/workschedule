package com.GrzegorzKarolak.workschedule.schedulegenerator.builder

import com.GrzegorzKarolak.workschedule.schedulegenerator.domain.ScheduleSettings
import groovy.transform.CompileStatic
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

import java.time.YearMonth

@CompileStatic
@Builder(builderStrategy = SimpleStrategy, prefix = "with")
class ScheduleSettingsBuilder {

    Map<YearMonth, Integer> fullTimeWorkingTimePerMonthInHours

    static ScheduleSettingsBuilder aScheduleSettings() {
        return new ScheduleSettingsBuilder()
    }

    ScheduleSettings build() {
        return new ScheduleSettings (fullTimeWorkingTimePerMonthInHours)
    }
}

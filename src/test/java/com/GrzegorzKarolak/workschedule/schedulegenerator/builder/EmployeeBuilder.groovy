package com.GrzegorzKarolak.workschedule.schedulegenerator.builder

import com.GrzegorzKarolak.workschedule.schedulegenerator.domain.Employee
import groovy.transform.CompileStatic
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy

import java.time.LocalDate

@CompileStatic
@Builder(builderStrategy = SimpleStrategy, prefix = "with")
class EmployeeBuilder {
    String firstName
    String lastName
    Double workingTimePart
    List<LocalDate> holidayDays;

    static EmployeeBuilder aEmployee() {
        return new EmployeeBuilder()
    }

    Employee build() {
        return new Employee(firstName, lastName, workingTimePart, holidayDays)
    }
}

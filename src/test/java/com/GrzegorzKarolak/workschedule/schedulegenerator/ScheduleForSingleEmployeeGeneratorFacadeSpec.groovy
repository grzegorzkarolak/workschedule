package com.GrzegorzKarolak.workschedule.schedulegenerator

import com.GrzegorzKarolak.workschedule.schedulegenerator.domain.Employee
import com.GrzegorzKarolak.workschedule.schedulegenerator.domain.ScheduleForSingleEmployee
import com.GrzegorzKarolak.workschedule.schedulegenerator.domain.TimePeriod
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import static com.GrzegorzKarolak.workschedule.schedulegenerator.builder.EmployeeBuilder.aEmployee

class ScheduleForSingleEmployeeGeneratorFacadeSpec extends Specification {

    @Subject
    ScheduleGeneratorFacade scheduleGeneratorFacade = new ScheduleGeneratorFacade()

    def "Should generate work schedule for 1 employee, for various days, regard to employee's holiday"() {
        given: "1 employee 'John Smith', " +
                "with hired full-time, " +
                "with holiday on 06-01-2020, 07-01-2020 and 08-01-2020"
        List<LocalDate> employeesHolidayDays = new ArrayList<>()
        employeesHolidayDays.addAll([LocalDate.of(2020, 1, 6),
                                     LocalDate.of(2020, 1, 7),
                                     LocalDate.of(2020, 1, 8)])
        Employee employee = aEmployee()
                .withFirstName("John")
                .withLastName("Smith")
                .withWorkingTimePart(1)
                .withHolidayDays(employeesHolidayDays)
                .build()

        when:   "for calculation" +
                "use period of time to generate schedule: 01-01-2020 - 09-02-2020, " +
                "shift has 8 hours per day"
        TimePeriod requestedPeriod = new TimePeriod(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 02, 9))
        ScheduleForSingleEmployee calculatedSchedule = scheduleGeneratorFacade.calculateFor(employee, requestedPeriod, 8, ScheduleSettings)

        then: "generated schedule has set employee 'John Smith'"
        calculatedSchedule.getEmployee().getFirstName() == "John"
        calculatedSchedule.getEmployee().getLastName() == "Smith"

        and: "generated schedule has 40 days"
        calculatedSchedule.getSchedule().size() == 40

        and: "generated schedule has set working days from 01-01-2020 to 22-01-2020 without 06-01-2020, 07-01-2020, 08-01-2020"
        calendarToString(calculatedSchedule.getSchedule().get(0).getDay()) == "01-01-2020"
        calculatedSchedule.getSchedule().get(0).getWork() == true
        calendarToString(calculatedSchedule.getSchedule().get(1).getDay()) == "02-01-2020"
        calculatedSchedule.getSchedule().get(1).getWork() == true
        calendarToString(calculatedSchedule.getSchedule().get(4).getDay()) == "05-01-2020"
        calculatedSchedule.getSchedule().get(4).getWork() == true
        calendarToString(calculatedSchedule.getSchedule().get(5).getDay()) == "06-01-2020"
        calculatedSchedule.getSchedule().get(5).getWork() == false
        calendarToString(calculatedSchedule.getSchedule().get(6).getDay()) == "07-01-2020"
        calculatedSchedule.getSchedule().get(6).getWork() == false
        calendarToString(calculatedSchedule.getSchedule().get(7).getDay()) == "08-01-2020"
        calculatedSchedule.getSchedule().get(7).getWork() == false
        calendarToString(calculatedSchedule.getSchedule().get(8).getDay()) == "09-01-2020"
        calculatedSchedule.getSchedule().get(8).getWork() == true
        calendarToString(calculatedSchedule.getSchedule().get(23).getDay()) == "24-01-2020"
        calculatedSchedule.getSchedule().get(23).getWork() == true
        calendarToString(calculatedSchedule.getSchedule().get(24).getDay()) == "25-01-2020"
        calculatedSchedule.getSchedule().get(24).getWork() == false
        calendarToString(calculatedSchedule.getSchedule().get(29).getDay()) == "30-01-2020"
        calculatedSchedule.getSchedule().get(29).getWork() == false
        calendarToString(calculatedSchedule.getSchedule().get(30).getDay()) == "31-01-2020"
        calculatedSchedule.getSchedule().get(30).getWork() == false
    }

    static String calendarToString (LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String result = date.format(formatter)
        return result
    }

}
